<?php

namespace WSIServices\PhpCli;

use WSIServices\Common\Configuration\NonReferenceTrait,
	WSIServices\Common\Configuration\StrictTrait,
	WSIServices\Common\Configuration\LoadTrait;

/**
 * Description of Cli
 *
 * @author Sam Likins
 */
class Cli {

	use NonReferenceTrait,
		StrictTrait,
		LoadTrait;

	protected $optionTypes = array(
		'value',
		'setup',
		'definition',
		'definitionValue',
	);

	protected $options = array();

	protected $optionAliases = array();

	public function setOptionTypes($types = null) {
		$defaultTypes = array('value', 'setup', 'definition', 'definitionValue');

		if($types === null)
			$this->optionTypes = $defaultTypes;
		elseif(!is_array($types))
			throw new \UnexpectedValueException('The types paramiter must be an array.');
		else {
			if(count(array_intersect_key(array_flip($defaultTypes), array_flip($types))) !== 4)
				throw new \InvalidArgumentException('The types array must contain keys `'.implode('`, `', $defaultTypes).'` at a minimum.');

			$this->optionTypes = $types;
		}
	}

	public function setOption($option, $config) {
		if(!is_string($option))
			throw new \UnexpectedValueException('The option paramiter must be a string.');

		if(!is_array($config))
			throw new \UnexpectedValueException('The config paramiter must be an array.');

		$optionTypes = array_fill_keys($this->optionTypes, null);

		$config = array_merge(
			$optionTypes,
			 array_intersect_key($config, $optionTypes)
		);

		if(array_key_exists($option, $this->options))
			$this->options[$option] = array_merge($this->options[$option], $config);
		else
			$this->options[$option] = $config;
	}

	public function unsetOption($option) {
		if(!is_string($option))
			throw new \UnexpectedValueException('The option paramiter must be a string.');

		if(!array_key_exists($option, $this->options))
			throw new \InvalidArgumentException('The option paramiter value `'.$option.'` is not defined.');

		unset($this->options[$option]);
	}

	public function setOptionAlias($option, $alias) {
		if(!is_string($option))
			throw new \UnexpectedValueException('The option paramiter must be a string.');

		if(!is_string($alias))
			throw new \UnexpectedValueException('The alias paramiter must be a string.');

		if(!array_key_exists($option, $this->options))
			throw new \InvalidArgumentException('The option paramiter value `'.$option.'` is not defined.');

		$this->optionAliases[$alias] = $option;
	}

	public function unsetOptionAlias($alias) {
		if(array_key_exists($alias, $this->optionAliases))
			unset($this->optionAliases[$alias]);
	}

	public function setOptionValue($option, $value, $useCallback = true) {
		if(!is_string($option))
			throw new \UnexpectedValueException('The option paramiter must be a string.');

		if(array_key_exists($option, $this->optionAliases))
			$option = $this->optionAliases[$option];

		if(!array_key_exists($option, $this->options))
			throw new \InvalidArgumentException('The option paramiter value `'.$option.'` is not defined nor an alias.');

		if($useCallback === false)
			$this->options[$option]['value'] = $value;
		elseif($useCallback === true && is_callable($this->options[$option]['setup']))
			$useCallback = $this->options[$option]['setup'];
		elseif(!is_callable($useCallback))
			throw new \UnexpectedValueException('The useCallback paramiter must be boolean or callable');

		$result = $useCallback($value, $option);
		if($result !== null)
			$this->options[$option]['value'] = $value;
	}

	public function getOptionValue($option) {
		if(!array_key_exists($option, $this->options))
			throw new \InvalidArgumentException('The option paramiter value `'.$option.'` is not defined.');

		return $this->options[$option]['value'];
	}

}